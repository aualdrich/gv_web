	$("#welcome_target_type").change(function() {target_type_changed();});	
	
	var edit_mode = "<%= @view_mode %>";
	
	if(edit_mode == "save")
		populate_dropdown("get_episodes", "episode_content", "Target Episode");
	
	else {
		 target_type_changed();
		 set_selected_value_for_target_type();
	}	
	
	function target_type_changed() {
		
		var target_type = $("#welcome_target_type").val();		
		
		switch(target_type) {				
			case "Video Clip":
				populate_dropdown("get_clips", "clip_content", "Target Clip");
			break;
					
			case "Episode":
				populate_dropdown("get_episodes", "episode_content", "Target Episode");
			break;

			case "Image":
				populate_link_dropdown("image_link_content", "Image Link");
			break;
			
			case "Link":
				populate_link_dropdown("link_content", "Link");
			break;	
			
			case "Profile":
				populate_dropdown("get_profiles", "profile_content", "Target Profile");
			break;
			
			case "Venture":
				populate_dropdown("get_ventures", "venture_content", "Target Venture");
			break;
				
		}
	}
		
	function populate_dropdown(action_name, select_id, label_text) {
			$.getJSON("/welcome/" + action_name,
			 function(data) {			
				var select_html = "<select id='" + select_id + "' name ='" + select_id + "'>";
				$.each(data, function(key, val)
					{						
						select_html += "<option value='" + val.id + "'>" + val.name + "</option>";
					});
				select_html += "</select>";		
				
				var label_html = "<label for='" + select_id + "'>" + label_text + "</label>";
				
				$("#target_type_content").html(label_html + select_html);											
			});
	}
	
	function populate_link_dropdown(id, label_text) {
		var label_html = "<label for='link_content'>" + label_text + "</label>";
		var link_html = "<input type='text' id='" + id + "' name='" + id + "' />";
		
		$("#target_type_content").html(label_html + link_html);
	}
	
	function set_selected_value_for_target_type()
	{
		var target_type = $("#welcome_target_type").val();	
		
		switch(target_type) {				
			case "Video Clip":
				set_selected_dropdown_value("get_welcome_video_clip", "clip_content");
			break;
					
			case "Episode":
				set_selected_dropdown_value("get_welcome_episode", "episode_content");
			break;

			case "Image":
				set_selected_image_value();
			break;
			
			case "Link":
				set_selected_link_value();
			break;		
			
			case "Profile":
				set_selected_dropdown_value("get_welcome_profile", "profile_content");
			break;
			
			case "Venture":
				set_selected_dropdown_value("get_welcome_venture", "venture_content");
			break;
			
		}
	}
	
	function set_selected_dropdown_value(action_name, content_id) {
		$.getJSON("/welcome/" + action_name,
			function(data) {
				var selected_id = data;
			
			$("#" + content_id + " option[value='" + selected_id + "']").attr("selected", "selected");
			
			});
	}
		
	function set_selected_image_value() {
		$.getJSON("/welcome/get_welcome_image_link",
			function(data) {
				var image_link = data.name;
				$("#image_link_content").val(image_link);			
			});
	}
	
	function set_selected_link_value() {
		$.getJSON("/welcome/get_welcome_link",
			function(data) {
				var link = data.name
				$("#link_content").val(link);			
			});
	}