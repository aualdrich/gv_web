class Photo < ActiveRecord::Base

  attr_accessible :photo

  validates_attachment_presence :photo

  has_attached_file :photo,
                    :styles => { :detail => "600x640", :thumb => "120x128" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :bucket => 'global_ventures'
  has_many :photos_ventures
  has_many :ventures, :through => :photos_ventures
  has_many :photos_profiles
  has_many :profiles, :through => :photos_profiles

  def as_json(*args)
    hash = super(*args)

    photo_hash = {:url => photo.url(:detail)}

    hash.merge!(photo_hash)
  end


end
