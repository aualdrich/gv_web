class Venture < ActiveRecord::Base
  attr_accessible :country_info, :date_end, 
    :date_start, :link,
    :name, :num_recorded_healings, :num_salvations, :price, :venture_type, :episode_id, :photos, :main_photo_path, :main_thumb_photo_path, :order_num, :num_people_on_trip
  
  validates :name, :country_info, :date_start, :date_end, :link, :venture_type, :presence => "true"
    
  has_many :photos_ventures
  has_many :photos, :through => :photos_ventures
  belongs_to :episode
  belongs_to :welcome

  has_attached_file :main_photo_path, :styles => { :detail => "640x360",
                                                   :thumb => "280x160" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :bucket => 'global_ventures'

  has_attached_file :main_thumb_photo_path, :styles => { :thumb => "160x160" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :bucket => 'global_ventures'

  def as_json(*args)
    hash = super(*args)

    main_photo_path_hash = {:main_photo_path_thumb => main_thumb_photo_path.url(:thumb),
                            :main_photo_path_detail => main_photo_path.url(:detail)}

    hash.merge!(main_photo_path_hash)
  end

end
