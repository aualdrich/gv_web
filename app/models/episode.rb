class Episode < ActiveRecord::Base
  validates :air_date, :description, :link, :name, :season_num, :order_num, :presence => true
  validates :episode_num, :presence => true, :uniqueness => {:scope => :season_num }
  attr_accessible :air_date, :description, :episode_num, :link, :name, :season_num, :main_photo_path, :main_thumb_photo_path,:order_num
  has_many :clips
  has_many :ventures
  belongs_to :welcome
  has_attached_file :main_photo_path, :styles => { :detail => "640x360",
                                                   :thumb => "280x160" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :bucket => 'global_ventures'

  has_attached_file :main_thumb_photo_path, :styles => { :thumb => "160x160" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :bucket => 'global_ventures'


  def as_json(*args)
    hash = super(*args)

    main_photo_path_hash = {:main_photo_path_thumb => main_thumb_photo_path.url(:thumb),
                            :main_photo_path_detail => main_photo_path.url(:detail)}

    hash.merge!(main_photo_path_hash)
  end


end
