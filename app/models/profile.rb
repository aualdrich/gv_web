class Profile < ActiveRecord::Base
  validates :bio, :more, :num_countries, :num_gv_trips, :quote, :random, :role, :presence => true
  validates :name, :presence => true, :uniqueness => true
  attr_accessible :bio, :more, :name, :num_countries, :num_gv_trips, :quote, :random, :role, :main_photo_path, :main_thumb_photo_path, :order_num
  has_attached_file :main_photo_path, :styles => { :detail => "640x360",
                                                   :thumb => "280x160" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :bucket => 'global_ventures'

  has_attached_file :main_thumb_photo_path, :styles => { :thumb => "160x160" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :bucket => 'global_ventures'
  
  has_many :photos_profiles
  has_many :photos, :through => :photos_profiles
  belongs_to :welcome

  def as_json(*args)
    hash = super(*args)

    main_photo_path_hash = {:main_photo_path_thumb => main_thumb_photo_path.url(:thumb),
                            :main_photo_path_detail => main_photo_path.url(:detail)}

    hash.merge!(main_photo_path_hash)
  end
  
end
