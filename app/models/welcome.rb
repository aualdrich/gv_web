class Welcome < ActiveRecord::Base
  attr_accessible :description, :name, :target_type, :link, :image_link, :header_text, :main_photo_path
  validates :description, :target_type, :presence => true
  belongs_to :episode
  belongs_to :clip
  belongs_to :profile
  belongs_to :venture
  has_attached_file :main_photo_path, :styles => { :detail => "640x360",
                                                   :thumb => "280x160" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :bucket => 'global_ventures'


  def as_json(*args)
    hash = super(*args)

    main_photo_path_hash = {:main_photo_path_thumb => main_photo_path.url(:thumb),
                            :main_photo_path_detail => main_photo_path.url(:detail)}

    hash.merge!(main_photo_path_hash)
  end
end
