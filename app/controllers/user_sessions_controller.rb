class UserSessionsController < ApplicationController
  before_filter :require_user, :only => "logout"

  def login
    @user_session = UserSession.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])

      if @user_session.save
        redirect_to :action => "index", :controller => "home"
      else
        render :action => 'login'
      end
  end

  def logout
    @user_session = UserSession.find
    @user_session.destroy

    redirect_to :action => 'index', :controller => 'home'
  end

end
