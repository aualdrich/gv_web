class WelcomeController < ApplicationController
  before_filter :require_user, :only => ['index', 'create', 'modify', 'update', 'save']
  def index
    @welcome = Welcome.first  
  end
  
  def create
    @welcome = Welcome.new
    @view_mode = 'save'
  end
  
  def modify
    @welcome = Welcome.first
    @view_mode = 'update'
    
    render :action => 'create'
  end
  
  def update     

    @welcome = Welcome.find(params[:id])
    
    tmp = Welcome.new(params[:welcome])
  
    update_content_record(tmp.target_type, params)
      
    if(@welcome).update_attributes(params[:welcome])      
      
      redirect_to :action => 'index'
    else
      @view_mode = "update"
      render :action => 'create'
    end
    
  end
  
  def save   
    @welcome = Welcome.new(params[:welcome])
    
    update_content_record(@welcome.target_type, params)
        
    if @welcome.save       
      #remove the old welcome entry if one exists     
      if(Welcome.count > 1)
        @old_welcome = Welcome.first
        @old_welcome.delete 
      end
    
      redirect_to :action => 'index'
    else
      @view_mode = "save"
      render :action => 'create'
    end
  end
  
  def update_content_record(target_type, params)    

      @welcome.episode = nil
      @welcome.clip = nil
      @welcome.image_link = nil
      @welcome.link = nil
      @welcome.profile = nil
      @welcome.venture = nil

      case target_type      
        when "Episode"
          episode = Episode.find(params[:episode_content])
          @welcome.episode = episode
        when "Image"
          image_link = params[:image_link_content]
          @welcome.image_link = image_link
        when "Link"
          @welcome.link = params[:link_content] 
        when "Video Clip"
          clip = Clip.find(params[:clip_content])
          @welcome.clip = clip
        when "Profile"
          profile = Profile.find(params[:profile_content])
          @welcome.profile = profile
        when "Venture"
          venture = Venture.find(params[:venture_content])
          @welcome.venture = venture          
      end
    
  end
  
  
  def get_welcome
    @welcome = Welcome.first  
    render :json => @welcome
  end
  
  def get_sorted_episodes
     episodes = Episode.all(:order => "season_num ASC, episode_num ASC")
  end
  
  def get_episodes
    episodes = get_sorted_episodes()
    
    @formatted_episodes = Array.new
    
    episodes.each do |e|
        formatted_episode = SelectList.new
        formatted_episode.name = "#{e.season_num}.#{e.episode_num} #{e.name}"
        formatted_episode.id = e.id
  
        @formatted_episodes << formatted_episode
    end
    
    render :json => @formatted_episodes
  end
  
  def get_clips
    episodes = get_sorted_episodes()
        
    @clips = Array.new
    
    episodes.each do |e|        
      sorted_clips = e.clips.all(:order => "name ASC")
               
      sorted_clips.each do |c|
        clip = SelectList.new
        clip.name = "#{e.season_num}.#{e.episode_num} #{e.name} - #{c.name}"
        clip.id = c.id
        
        @clips << clip        
      end
    end
    
    render :json => @clips
  end
  
  def get_profiles
    @profiles = get_sorted_profiles()
    
    render :json => @profiles
    
  end

  def get_sorted_profiles
    profiles = Profile.order("name")
  end
  
  def get_ventures
    @ventures = get_sorted_ventures()
    
    render :json => @ventures
  end
  
  def get_sorted_ventures
    sorted_ventures = Venture.order("name")
  end

    
  def get_welcome_video_clip_data
    @clip = Welcome.first.clip
  end
  
  def get_welcome_episode_data
    @episode = Welcome.first.episode
  end
  
  def get_welcome_image_link_data
    @link = Welcome.first.image_link
  end
  
  def get_welcome_profile_data
    @profile = Welcome.first.profile
  end
  
  def get_welcome_venture_data
    @venture = Welcome.first.venture
  end
  
  def get_welcome_link_data
    @link = Welcome.first.link
  end
  
  def get_welcome_profile_data
    @profie = Welcome.first.profile
  end
  
  def get_welcome_venture_data
    @venture = Welcome.first.venture
  end
  
  def get_welcome_video_clip
      @clip = get_welcome_video_clip_data()

      render :json => @clip.id
  end
  
  def get_welcome_episode
    @episode = get_welcome_episode_data()
    
    render :json => @episode.id
  end
  
  def get_welcome_image_link    
    link = get_welcome_image_link_data()
    
    @link_data = SelectList.new    
    @link_data.name = link

    render :json => @link_data
  end
  
  def get_welcome_link
    link = get_welcome_link_data()
    
    @link_data = SelectList.new    
    @link_data.name = link

    render :json => @link_data
    
  end
  
  def get_welcome_profile
    @profile = get_welcome_profile_data()
    
    render :json => @profile.id
  end
  
  def get_welcome_venture
    @venture = get_welcome_venture_data()
    
    render :json => @venture.id
  end
  
end
