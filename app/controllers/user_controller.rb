class UserController < ApplicationController
  before_filter :require_user

  def index
    @users = User.order(:email)
  end



  def register
    @user = User.new
  end


  def create
    @user = User.new(params[:user])

    if @user.save
      redirect_to :action => 'index'
    else
      render :action => 'register'
    end

  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(params[:user])
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end

  end

  def delete
    @user = User.find(params[:id])
    @user.destroy

    redirect_to :action => 'index'
  end


end
