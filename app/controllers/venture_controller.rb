class VentureController < ApplicationController
  before_filter :require_user, :except => ['get_episodes', 'get_upcoming_ventures', 'get_previous_ventures', 'get_venture_photos', 'get_venture_by_id']

  def index
    @venture_settings = VentureSettings.first
    order_by = get_sort_order_for_ventures @venture_settings

    @upcoming = Venture.where(:venture_type => "Upcoming").order(order_by)
    @previous = Venture.where(:venture_type => "Previous").order(order_by)
  end

  def create
    @venture = Venture.new
    @view_mode = "save"
    @episodes = get_episodes()

  end

  def edit
    @venture = Venture.find(params[:venture_id])
    @view_mode = "update"
    @episodes = get_episodes()

    render :action => "create"

  end

  def get_episodes()
    episodes = Episode.order("season_num ASC, episode_num ASC")
  end

  def save
    @venture = Venture.new(params[:venture])

    if (@venture.save)
      redirect_to :action => "index"

    else
      @view_mode = "save"
      @episodes = get_episodes()
      render :action => "create"
    end
  end

  def update
    @venture = Venture.find(params[:venture_id])

    if (@venture.update_attributes(params[:venture]))
      redirect_to :action => "index"
    else
      @view_mode = "update"
      @episodes = get_episodes()
      render :action => "create"
    end
  end

  def delete
    @venture = Venture.find(params[:venture_id])

    Venture.delete(@venture)

    redirect_to :action => "index"

  end

  def add_photo
    @venture = Venture.find(params[:id])
    @photo = Photo.new
  end

  def save_photo
    @venture = Venture.find(params[:id])
    @venture.photos.new(params[:photo])

    if @venture.save
      redirect_to :action => 'manage_photos', :id => @venture.id
    else
      @photo = Photo.new
      render :action => 'add_photo'
    end


  end

  def manage_photos
    @venture = Venture.find(params[:id])

  end

  def delete_photo
    @photo = Photo.find(params[:id])
    @photo.destroy

    redirect_to :action => 'manage_photos', :id => params[:venture_id]
  end

  def edit_venture_settings
    @venture_settings = VentureSettings.first
  end

  def update_venture_settings
    @venture_settings = VentureSettings.find(params[:id])

    @venture_settings.update_attributes(params[:venture_settings])

    redirect_to :action => "index"
  end


  def get_upcoming_ventures
    venture_settings = VentureSettings.first
    sort_order = get_sort_order_for_ventures venture_settings
    @upcoming = Venture.where(:venture_type => "Upcoming").order(sort_order)
    render :json => @upcoming

  end

  def get_previous_ventures
    venture_settings = VentureSettings.first
    sort_order = get_sort_order_for_ventures venture_settings
    @previous = Venture.where(:venture_type => "Previous").order(sort_order)
    render :json => @previous
  end

  def get_venture_photos
    venture = Venture.find(params[:venture_id])
    @photos = venture.photos
    render :json => @photos
  end

  def get_venture_by_id
    @venture = Venture.find(params[:venture_id])
    render :json => @venture
  end

  def get_venture_settings
    @venture_settings = VentureSettings.first
    render :json => @venture_settings
  end

  def get_sort_order_for_ventures venture_settings
    venture_settings.allow_custom_sorting ? "order_num" : "name"
  end

end