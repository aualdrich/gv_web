class ProfileController < ApplicationController
  before_filter :require_user, :except => ['get_profiles', 'get_profile_photos', 'get_profile_by_id']

  def index
    @profile_settings = ProfileSettings.first

    order_by = get_sort_order_for_profiles @profile_settings

    @profiles = Profile.order(order_by)

  end
  
  def create
    @profile = Profile.new
    @view_mode = "save"
    render :action => "create"
  end
  
  def edit
    @profile = Profile.find(params[:profile_id])
    @view_mode = "update"
    render :action => "create"
  end
  
  def save
    @profile = Profile.new(params[:profile])
    
    if(@profile.save)
      redirect_to :action => "index"
    else
      @view_mode = "save"
      render :action => "create"
    end
    
  end
  
  def update
    @profile = Profile.find(params[:id])
    
    if(@profile.update_attributes(params[:profile]))
      redirect_to :action => "index"
    else
      @view_mode = "update"
      render :action => "create"
    end
  end
  
  def delete
    @profile = Profile.find(params[:profile_id])
    @profile.destroy
    redirect_to :action => "index"    
  end

  def add_photo
    @profile = Profile.find(params[:id])
     @photo = Photo.new
  end

  def save_photo
    @profile = Profile.find(params[:id])
    @profile.photos.new(params[:photo])

    if @profile.save
      redirect_to :action => 'manage_photos', :id => @profile.id
    else
      @photo = Photo.new
      render :action => 'add_photo'
    end


  end

  def manage_photos
    @profile = Profile.find(params[:id])

  end

  def delete_photo
    @photo = Photo.find(params[:id])
    @photo.destroy

    redirect_to :action => 'manage_photos', :id => params[:profile_id]
  end


  def edit_profile_settings
    @profile_settings = ProfileSettings.first
  end

  def update_profile_settings
    @profile_settings = ProfileSettings.find(params[:id])

    @profile_settings.update_attributes(params[:profile_settings])

    redirect_to :action => "index"

  end

  def get_profiles
    profile_settings = ProfileSettings.first
    sort_order = get_sort_order_for_profiles profile_settings
    @profiles = Profile.order(sort_order)
    render :json => @profiles
  end
  
  def get_profile_photos
    profile = Profile.find(params[:profile_id])
    @photos = profile.photos
    
    render :json => @photos
  end
  
  def get_profile_by_id 
    @profile = Profile.find(params[:profile_id])
    
    render :json => @profile
  end

  def get_profile_settings
    @profile_settings = ProfileSettings.first
    render :json => @profile_settings
  end

  def get_sort_order_for_profiles(profile_settings)
    profile_settings.allow_custom_sorting ? "order_num" : "name"
  end






end