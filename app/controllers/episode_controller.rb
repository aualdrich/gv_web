class EpisodeController < ApplicationController
  before_filter :require_user, :except => ['get_episodes', 'get_episode_by_id']
  
  def index
    @episode_settings = EpisodeSettings.first
    order_filter = get_sort_order_for_episodes @episode_settings
    @episodes = Episode.order(order_filter)
  end

  def create
    @episode = Episode.new
    @view_mode = "save"
  end
  
  def edit
    @episode = Episode.find(params[:episode_id])
    @view_mode = "update"
    
    render :action => 'create'
  end
  
  def delete
    Episode.delete(params[:episode_id])    
    redirect_to :action => 'index'

  end

  def save
    @episode = Episode.new(params[:episode])  
  
    if @episode.save
      redirect_to :action => 'index'
    else
      @view_mode = "save"
      render :action => 'create'
    end
  end 
  
  def update
    @episode = Episode.find(params[:id])    
        
    if(@episode.update_attributes(params[:episode]))
      redirect_to :action => 'index'
    else
      @view_mode = "update"
      render :action => 'create'
    end
    
  end


  def edit_episode_settings
    @episode_settings = EpisodeSettings.first
  end

  def update_episode_settings
    @episode_settings = EpisodeSettings.find(params[:id])

    @episode_settings.update_attributes(params[:episode_settings])

    redirect_to :action => "index"

  end
  
  def get_episodes

    start_date = Time.now

    episode_settings = EpisodeSettings.first
    sort_order = get_sort_order_for_episodes episode_settings
    @episodes = Episode.order(sort_order)

    render :json => @episodes
  end

  def get_episode_by_id
    @episode = Episode.find(params[:episode_id])    
    render :json => @episode
  end

  def get_episode_settings
    @episode_settings = EpisodeSettings.first
    render :json => @episode_settings
  end

  def get_sort_order_for_episodes(episode_settings)
    order_filter = episode_settings.allow_custom_sorting ? "order_num ASC" : "season_num DESC, episode_num DESC"
  end
  
end
