class VenturePhotosController < ApplicationController
  # GET /venture_photos
  # GET /venture_photos.json
  def index
    @venture_photos = VenturePhoto.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @venture_photos }
    end
  end

  # GET /venture_photos/1
  # GET /venture_photos/1.json
  def show
    @venture_photo = VenturePhoto.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @venture_photo }
    end
  end

  # GET /venture_photos/new
  # GET /venture_photos/new.json
  def new
    @venture_photo = VenturePhoto.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @venture_photo }
    end
  end

  # GET /venture_photos/1/edit
  def edit
    @venture_photo = VenturePhoto.find(params[:id])
  end

  # POST /venture_photos
  # POST /venture_photos.json
  def create
    @venture_photo = VenturePhoto.new(params[:venture_photo])

    respond_to do |format|
      if @venture_photo.save
        format.html { redirect_to @venture_photo, notice: 'Venture photo was successfully created.' }
        format.json { render json: @venture_photo, status: :created, location: @venture_photo }
      else
        format.html { render action: "new" }
        format.json { render json: @venture_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /venture_photos/1
  # PUT /venture_photos/1.json
  def update
    @venture_photo = VenturePhoto.find(params[:id])

    respond_to do |format|
      if @venture_photo.update_attributes(params[:venture_photo])
        format.html { redirect_to @venture_photo, notice: 'Venture photo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @venture_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /venture_photos/1
  # DELETE /venture_photos/1.json
  def destroy
    @venture_photo = VenturePhoto.find(params[:id])
    @venture_photo.destroy

    respond_to do |format|
      format.html { redirect_to venture_photos_url }
      format.json { head :no_content }
    end
  end
end
