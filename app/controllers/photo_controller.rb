class PhotoController < ApplicationController
  before_filter :require_user
  
  def manage_photos
    @photo_type = params[:photo_type]

    case @photo_type
      when "Venture" then
        @photos = Venture.find(params[:id]).photos

      when "Profile" then
        @photos = Profile.find(params[:id]).photos

    end

   end
  
  
  
end