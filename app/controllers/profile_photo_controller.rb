class ProfilePhotoController < ApplicationController
  before_filter :require_user
  
  def index
    @profile_id = params[:profile_id]
    profile = Profile.find(@profile_id)
    
    @photos = profile.profile_photos
  end
  
  def create
    @photo = ProfilePhoto.new
    @profile_id = params[:profile_id]
  end
  
  def save
    @photo = ProfilePhoto.new(params[:profile_photo])
    
    tmp_profile_id = params[:profile_id]    
    @photo.profile_id = tmp_profile_id
    
    if(@photo.save)
      redirect_to :action => "index", :profile_id => tmp_profile_id
    else
      @profile_id = tmp_profile_id
      render :action => "create"
    end
    
  end
  
  def delete
    
    @photo = ProfilePhoto.find(params[:id])    
    
    tmp_profile_id = @photo.profile_id    
    
    @photo.destroy
    
    redirect_to :action => "index", :profile_id => tmp_profile_id
    
  end
  
  
end