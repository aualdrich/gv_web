class ClipController < ApplicationController
  before_filter :require_user, :except => ['get_clips_for_episode', 'get_clip_by_id' ]
  
  def create
    @clip = Clip.new
    @episode_id = params[:episode_id]
    @view_mode = "save"
  end
  
  def edit
    @clip = Clip.find(params[:clip_id])
    @episode_id = @clip.episode_id
    @view_mode = 'update'
    render :action => "create"
  end

  def index
    @episode_id = params[:episode_id]
    episode = Episode.find(@episode_id)
    @clips = episode.clips;
  end
  
  def save
    @clip = Clip.new(params[:clip])
    tmp_episode_id = params[:episode_id]
    
    @clip.episode_id = tmp_episode_id
    
    if(@clip.save)
      redirect_to :action => "index", :episode_id => tmp_episode_id
    else
      @view_mode = "save"
      @episode_id = tmp_episode_id
      render :action => "create"
    end
  end
    
  def update
    @clip = Clip.find(params[:id])
    
    if(@clip.update_attributes(params[:clip]))
      redirect_to :action => "index", :episode_id => @clip.episode_id
    else
      @view_mode = "update"
      @episode_id = @clip.episode_id
      render :action => "create"
    end
  end
  
  def delete
    @clip = Clip.find(params[:clip_id])
    episode_id = @clip.episode_id

    
    @clip.destroy
    
    redirect_to :action => "index", :episode_id => episode_id
  end
  
  def get_clips_for_episode
    @clips = Clip.where(:episode_id => params[:episode_id])
    
    render :json => @clips
  end
  
  def get_clip_by_id
    @clip = Clip.find(params[:clip_id])
    
    render :json => @clip
  end
  

end