class CreatePhotosVentures < ActiveRecord::Migration
  def change
    create_table :photos_ventures do |t|
      t.integer :venture_id
      t.integer :photo_id
    end
  end

end
