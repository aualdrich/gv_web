class AddThumbPathToEpisodes < ActiveRecord::Migration
  def up
    add_attachment :episodes, :main_thumb_photo_path
  end

  def down
    remove_attachment :episodes, :main_thumb_photo_path
  end
end
