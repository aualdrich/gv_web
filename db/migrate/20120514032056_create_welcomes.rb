class CreateWelcomes < ActiveRecord::Migration
  def change
    create_table :welcomes do |t|
      t.string :text
      t.string :name
      t.string :description
      t.string :target_type
      t.integer :episode_id
      t.string :photo_path
      t.integer :clip_id
      t.string :link
      t.string :image_link
      t.integer :profile_id
      t.integer :venture_id

      t.timestamps
    end
  end
end
