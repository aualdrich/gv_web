class RemovePhotoPathColumnFromProfiles < ActiveRecord::Migration
  def change
    remove_column :profiles, :photo_path
  end
end
