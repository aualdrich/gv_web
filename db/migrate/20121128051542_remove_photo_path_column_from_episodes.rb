class RemovePhotoPathColumnFromEpisodes < ActiveRecord::Migration
  def change
    remove_column :episodes, :photo_path
  end
end
