class CreatePhotosProfiles < ActiveRecord::Migration
  def change
    create_table :photos_profiles do |t|
      t.integer :profile_id
      t.integer :photo_id
    end
  end
end
