class AddMainPhotoPathColumnsToEpisodes < ActiveRecord::Migration
  def self.up
    add_attachment :episodes, :main_photo_path
  end


  def self.down
    remove_attachment :episodes, :main_photo_path
  end

end
