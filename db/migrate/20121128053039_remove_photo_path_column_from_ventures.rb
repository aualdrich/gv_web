class RemovePhotoPathColumnFromVentures < ActiveRecord::Migration
  def change
    remove_column :ventures, :photo_path
  end
end
