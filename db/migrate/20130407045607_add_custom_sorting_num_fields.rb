class AddCustomSortingNumFields < ActiveRecord::Migration
  def change
    add_column :episodes, :order_num, :integer
    add_column :profiles, :order_num, :integer
    add_column :ventures, :order_num, :integer
  end
end
