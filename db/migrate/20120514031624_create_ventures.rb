class CreateVentures < ActiveRecord::Migration
  def change
    create_table :ventures do |t|
      t.string :name
      t.string :photo_path
      t.integer :num_salvations
      t.integer :num_recorded_healings
      t.integer :episode_id
      t.string :venture_type
      t.datetime :date_start
      t.datetime :date_end
      t.decimal :price
      t.string :country_info
      t.string :link

      t.timestamps
    end
  end
end
