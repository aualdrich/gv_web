class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :photo_path
      t.string :role
      t.string :bio
      t.integer :num_countries
      t.integer :num_gv_trips
      t.string :quote
      t.string :random
      t.string :more
      

      t.timestamps
    end
  end
end
