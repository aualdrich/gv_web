class AddThumbPathToProfiles < ActiveRecord::Migration
  def up
    add_attachment :profiles, :main_thumb_photo_path
  end

  def down
    remove_attachment :profiles, :main_thumb_photo_path
  end
end
