class AddNumPeopleOnTripToVentures < ActiveRecord::Migration
  def change
    add_column :ventures, :num_people_on_trip, :integer
  end
end
