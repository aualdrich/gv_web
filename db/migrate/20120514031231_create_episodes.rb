class CreateEpisodes < ActiveRecord::Migration
  def change
    create_table :episodes do |t|
      t.string :name
      t.string :description
      t.integer :season_num
      t.integer :episode_num
      t.datetime :air_date
      t.string :link
      t.string :photo_path

      t.timestamps
    end
  end
end
