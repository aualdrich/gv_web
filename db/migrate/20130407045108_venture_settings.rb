class VentureSettings < ActiveRecord::Migration
  def change
    create_table :venture_settings do |vs|
      vs.boolean :allow_custom_sorting
    end
  end
end
