class CreateClips < ActiveRecord::Migration
  def change
    create_table :clips do |t|
      t.string :name
      t.string :description
      t.string :link
      t.string :photo_path
      t.integer :episode_id
      
      t.timestamps
    end
  end
end
