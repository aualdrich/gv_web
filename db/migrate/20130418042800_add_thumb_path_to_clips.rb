class AddThumbPathToClips < ActiveRecord::Migration
  def up
    add_attachment :clips, :main_thumb_photo_path
  end

  def down
    remove_attachment :clips, :main_thumb_photo_path
  end
end
