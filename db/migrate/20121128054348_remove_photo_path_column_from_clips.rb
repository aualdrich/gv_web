class RemovePhotoPathColumnFromClips < ActiveRecord::Migration
  def change
    remove_column :clips, :photo_path
  end
end
