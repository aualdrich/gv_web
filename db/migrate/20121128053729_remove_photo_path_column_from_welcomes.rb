class RemovePhotoPathColumnFromWelcomes < ActiveRecord::Migration
  def change
    remove_column :welcomes, :photo_path
  end
end
