class EpisodeSettings < ActiveRecord::Migration
  def change
    create_table :episode_settings do |es|
      es.boolean :allow_custom_sorting
    end
  end
end
