class AddMainPhotoPathColumnsToClips < ActiveRecord::Migration
  def self.up
    add_attachment :clips, :main_photo_path
  end

  def self.down
    remove_attachment :clips, :main_photo_path
  end
end
