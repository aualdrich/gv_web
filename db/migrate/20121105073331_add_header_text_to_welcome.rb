class AddHeaderTextToWelcome < ActiveRecord::Migration
  def change
    add_column :welcomes, :header_text, :string
  end
end
