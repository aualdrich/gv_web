class AddMainPhotoPathColumnsToWelcomes < ActiveRecord::Migration
  def self.up
    add_attachment :welcomes, :main_photo_path
  end

  def self.down
    remove_attachment :welcomes, :main_photo_path
  end
end
