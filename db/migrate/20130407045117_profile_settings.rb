class ProfileSettings < ActiveRecord::Migration
  def change

    create_table :profile_settings do |ps|
      ps.boolean :allow_custom_sorting
    end

  end
end
