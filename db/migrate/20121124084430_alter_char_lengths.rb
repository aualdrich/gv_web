class AlterCharLengths < ActiveRecord::Migration
  def up
    change_column :clips, :description, :text
    change_column :episodes, :description, :text
    change_column :profiles, :role, :text
    change_column :profiles, :bio, :text
    change_column :profiles, :quote, :text
    change_column :profiles, :random, :text
    change_column :profiles, :more, :text
    change_column :ventures, :country_info, :text
    change_column :welcomes, :description, :text

  end

  def down
  end
end
