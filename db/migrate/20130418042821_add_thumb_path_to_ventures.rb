class AddThumbPathToVentures < ActiveRecord::Migration
  def up
    add_attachment :ventures, :main_thumb_photo_path
  end

  def down
    remove_attachment :ventures, :main_thumb_photo_path
  end
end
