class AddMainPhotoPathColumnsToProfiles < ActiveRecord::Migration
    def self.up
      add_attachment :profiles, :main_photo_path
    end


    def self.down
      remove_attachment :profiles, :main_photo_path
    end

end
