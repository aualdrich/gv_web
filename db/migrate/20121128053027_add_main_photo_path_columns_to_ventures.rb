class AddMainPhotoPathColumnsToVentures < ActiveRecord::Migration
  def self.up
    add_attachment :ventures, :main_photo_path
  end


  def self.down
    remove_attachment :ventures, :main_photo_path
  end
end
