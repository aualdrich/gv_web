# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130425034441) do

  create_table "clips", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "link"
    t.integer  "episode_id"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "main_photo_path_file_name"
    t.string   "main_photo_path_content_type"
    t.integer  "main_photo_path_file_size"
    t.datetime "main_photo_path_updated_at"
    t.string   "thumb_photo_file_name"
    t.string   "thumb_photo_content_type"
    t.integer  "thumb_photo_file_size"
    t.datetime "thumb_photo_updated_at"
    t.string   "main_thumb_photo_path_file_name"
    t.string   "main_thumb_photo_path_content_type"
    t.integer  "main_thumb_photo_path_file_size"
    t.datetime "main_thumb_photo_path_updated_at"
  end

  create_table "episode_settings", :force => true do |t|
    t.boolean "allow_custom_sorting"
  end

  create_table "episodes", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "season_num"
    t.integer  "episode_num"
    t.datetime "air_date"
    t.string   "link"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "main_photo_path_file_name"
    t.string   "main_photo_path_content_type"
    t.integer  "main_photo_path_file_size"
    t.datetime "main_photo_path_updated_at"
    t.integer  "order_num"
    t.string   "main_thumb_photo_path_file_name"
    t.string   "main_thumb_photo_path_content_type"
    t.integer  "main_thumb_photo_path_file_size"
    t.datetime "main_thumb_photo_path_updated_at"
  end

  create_table "photos", :force => true do |t|
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "photos_profiles", :force => true do |t|
    t.integer "profile_id"
    t.integer "photo_id"
  end

  create_table "photos_ventures", :force => true do |t|
    t.integer "venture_id"
    t.integer "photo_id"
  end

  create_table "profile_settings", :force => true do |t|
    t.boolean "allow_custom_sorting"
  end

  create_table "profiles", :force => true do |t|
    t.string   "name"
    t.text     "role"
    t.text     "bio"
    t.integer  "num_countries"
    t.integer  "num_gv_trips"
    t.text     "quote"
    t.text     "random"
    t.text     "more"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "main_photo_path_file_name"
    t.string   "main_photo_path_content_type"
    t.integer  "main_photo_path_file_size"
    t.datetime "main_photo_path_updated_at"
    t.integer  "order_num"
    t.string   "thumb_photo_file_name"
    t.string   "thumb_photo_content_type"
    t.integer  "thumb_photo_file_size"
    t.datetime "thumb_photo_updated_at"
    t.string   "main_thumb_photo_path_file_name"
    t.string   "main_thumb_photo_path_content_type"
    t.integer  "main_thumb_photo_path_file_size"
    t.datetime "main_thumb_photo_path_updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "venture_settings", :force => true do |t|
    t.boolean "allow_custom_sorting"
  end

  create_table "ventures", :force => true do |t|
    t.string   "name"
    t.integer  "num_salvations"
    t.integer  "num_recorded_healings"
    t.integer  "episode_id"
    t.string   "venture_type"
    t.datetime "date_start"
    t.datetime "date_end"
    t.decimal  "price"
    t.text     "country_info"
    t.string   "link"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "main_photo_path_file_name"
    t.string   "main_photo_path_content_type"
    t.integer  "main_photo_path_file_size"
    t.datetime "main_photo_path_updated_at"
    t.integer  "order_num"
    t.string   "thumb_photo_file_name"
    t.string   "thumb_photo_content_type"
    t.integer  "thumb_photo_file_size"
    t.datetime "thumb_photo_updated_at"
    t.string   "main_thumb_photo_path_file_name"
    t.string   "main_thumb_photo_path_content_type"
    t.integer  "main_thumb_photo_path_file_size"
    t.datetime "main_thumb_photo_path_updated_at"
    t.integer  "num_people_on_trip"
  end

  create_table "welcomes", :force => true do |t|
    t.string   "text"
    t.string   "name"
    t.text     "description"
    t.string   "target_type"
    t.integer  "episode_id"
    t.integer  "clip_id"
    t.string   "link"
    t.string   "image_link"
    t.integer  "profile_id"
    t.integer  "venture_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "header_text"
    t.string   "main_photo_path_file_name"
    t.string   "main_photo_path_content_type"
    t.integer  "main_photo_path_file_size"
    t.datetime "main_photo_path_updated_at"
  end

end
