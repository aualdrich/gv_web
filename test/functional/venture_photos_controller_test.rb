require 'test_helper'

class VenturePhotosControllerTest < ActionController::TestCase
  setup do
    @venture_photo = venture_photos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:venture_photos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create venture_photo" do
    assert_difference('VenturePhoto.count') do
      post :create, venture_photo: { photo_path: @venture_photo.photo_path }
    end

    assert_redirected_to venture_photo_path(assigns(:venture_photo))
  end

  test "should show venture_photo" do
    get :show, id: @venture_photo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @venture_photo
    assert_response :success
  end

  test "should update venture_photo" do
    put :update, id: @venture_photo, venture_photo: { photo_path: @venture_photo.photo_path }
    assert_redirected_to venture_photo_path(assigns(:venture_photo))
  end

  test "should destroy venture_photo" do
    assert_difference('VenturePhoto.count', -1) do
      delete :destroy, id: @venture_photo
    end

    assert_redirected_to venture_photos_path
  end
end
